public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		KCalendar calendar = new KCalendar(this);
		
		setContentView(calendar);
		
		calendar.setOnCalendarClickListener(new OnCalendarClickListener() {
			@Override
			public void onCalendarClick(int row, int col, String dateFormat) {
				Toast.makeText(MainActivity.this, dateFormat, Toast.LENGTH_SHORT).show();
			}
		});
		
		calendar.setOnCalendarDateChangedListener(new OnCalendarDateChangedListener() {
			@Override
			public void onCalendarDateChanged(int year, int month) {
				Toast.makeText(MainActivity.this, year + "-" + month, Toast.LENGTH_SHORT).show();
			}
		});
	}
}